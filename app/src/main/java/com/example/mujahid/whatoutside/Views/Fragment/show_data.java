package com.example.mujahid.whatoutside.Views.Fragment;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.icu.util.TimeZone;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Activity.MainActivity;
import com.example.mujahid.whatoutside.Views.Activity.ManageLocation;
import com.example.mujahid.whatoutside.Views.Custom_Adapters.Horizontal_recycle_adapter;
import com.example.mujahid.whatoutside.Views.Custom_Adapters.Vertical_recyle_adapter;
import com.example.mujahid.whatoutside.Views.Helper.Converter;
import com.example.mujahid.whatoutside.Views.Helper.Utility;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetDailyWeather;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetFiveDayData;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.MainData;
import com.example.mujahid.whatoutside.Views.Model.LatLon;
import com.example.mujahid.whatoutside.Views.Model.ManageLocationPojo;
import com.example.mujahid.whatoutside.Views.Model.Test;
import com.example.mujahid.whatoutside.Views.Model.Test2;
import com.example.mujahid.whatoutside.Views.Model.TimeZonePojo;
import com.example.mujahid.whatoutside.Views.REST_Client.GetResponse;
import com.example.mujahid.whatoutside.Views.REST_Client.RetrofitClient;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.mujahid.whatoutside.Views.Helper.Utility.getExecute;

/**
 * A simple {@link Fragment} subclass.
 */
public class show_data extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    RecyclerView recyclerView;
    RecyclerView vertical_recyle;
    Horizontal_recycle_adapter adapter;
    Vertical_recyle_adapter vertical_recyle_adapter;
    List<GetDailyWeather.DailyWeather> list;
    List<Test2> vetical_list;

    //ViewHOlder
    TextView currentDate, day;
    TextView temp, min_temp, max_temp, humidity, weatherStatus, windSpeed;
    TextView pressure, cloud, rain, sunset, sunrise, country, population;
    ImageView mainImage;
    ProgressDialog dialog;

    SwipeRefreshLayout swipe;
    ManageLocationPojo locationPojo;

    CoordinatorLayout show_data_layout;
    String currentLocation;

    //test
    int small = 0;
    int large = 0;

    String timeZone;
    public show_data() {
        // Required empty public constructor
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_data, container, false);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void initViews(View view) {
        if (view != null) {
            temp = view.findViewById(R.id.mainTemp);
            min_temp = view.findViewById(R.id.min_temp);
            max_temp = view.findViewById(R.id.max_temp);
            weatherStatus = view.findViewById(R.id.textView15);
            humidity = view.findViewById(R.id.textView13);
            windSpeed = view.findViewById(R.id.textView12);
            mainImage = view.findViewById(R.id.imageView);

            pressure = view.findViewById(R.id.pressure);
            cloud = view.findViewById(R.id.cloud);
            rain = view.findViewById(R.id.rain);
            sunrise = view.findViewById(R.id.sunrise);
            sunset = view.findViewById(R.id.sunset);

            country = view.findViewById(R.id.country);
            population = view.findViewById(R.id.population);
            swipe = view.findViewById(R.id.swipe_view);
            show_data_layout = view.findViewById(R.id.show_data_layout);
        }
    }

    @SuppressLint("DefaultLocale")
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        if (getView() != null) {


            Bundle a = getArguments();
            locationPojo = (ManageLocationPojo) a.getSerializable("data");
            //currentLocation =  ((MainActivity)getActivity()).mTitle.getText().toString();
            assert locationPojo != null;
            currentLocation = locationPojo.getTitle();
            //getActivity().getActionBar().setTitle(locationPojo.getTitle());
            Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
            // TextView textView = toolbar.findViewById(R.id.toolbar_title);
            assert locationPojo != null;
            //  textView.setText(locationPojo.getTitle());
            if (isDataConnected(getActivity())) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getTimeZone(new LatLon(locationPojo.getLatitude(), locationPojo.getLongitude()));
                    }
                });
                thread.start();

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                getMainData(locationPojo.getLatitude(), locationPojo.getLongitude());
                getDailyData(locationPojo.getLatitude(), locationPojo.getLongitude(), 10);
                FiveDayData(locationPojo.getLatitude(), locationPojo.getLongitude());



            } else {
                ShowSnackBar("No internet connection");
            }


            //init view
            initViews(getView());

            swipe.setOnRefreshListener(this);


            recyclerView = getView().findViewById(R.id.horizontal_recycle);
            vertical_recyle = getView().findViewById(R.id.vertical_recyle);
            vertical_recyle.setLayoutManager(new LinearLayoutManager(getActivity()));
            vertical_recyle.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setHasFixedSize(true);
            recyclerView.setFocusable(true);
            recyclerView.setClickable(true);

            list = new ArrayList<>();


            //vertical list

            vetical_list = new ArrayList<>();

            vetical_list.add(new Test2("SUN 01 FEB", R.mipmap.w1d, 300, 310));
            vetical_list.add(new Test2("MON 02 FEB", R.mipmap.w1n, 304, 326));
            vetical_list.add(new Test2("TUE 03 FEB", R.mipmap.w1d, 298, 321));
            vetical_list.add(new Test2("WED 04 FEB", R.mipmap.w1n, 300, 329));


            //viewholder intilization

            currentDate = getView().findViewById(R.id.textView2);
            day = getView().findViewById(R.id.textView);
            day.setText(Utility.getCurrentDay());
            currentDate.setText(Utility.getCurrentDate(getActivity()));

        }



    }

    private boolean isDataConnected(Context context) {
        boolean isConnected = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo info = cm.getActiveNetworkInfo();
            final android.net.NetworkInfo wifi = cm
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            isConnected = info != null && info.isConnectedOrConnecting() || wifi.isConnectedOrConnecting();
        }
        return isConnected;
    }


    public void getTimeZone(LatLon latLon) {

        GetResponse response = RetrofitClient.getRetrofitForTimeZone().create(GetResponse.class);
        Call<TimeZonePojo> getZone = response.getTimeZone(latLon, 1331766000, "AIzaSyD0xcioJavbg3sdBPNylLK8aMBX6JXHj3U");
        try {
            Response<TimeZonePojo> response1 = getZone.execute();
            if (response1.body() != null) {
                timeZone = response1.body().getTimeZoneId();
                Log.d("StatusIs: ", response1.body().getStatus());
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getMainData(double lat, double lon) {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading......");
        dialog.show();

        GetResponse response = RetrofitClient.getRetrofit().create(GetResponse.class);
        Call<MainData> mainDataCall = response.getWeather(lon, lat, Utility.appid);

        mainDataCall.enqueue(new Callback<MainData>() {

            @SuppressLint("DefaultLocale")
            @Override
            public void onResponse(Call<MainData> call, @NonNull Response<MainData> response) {


                MainData data = response.body();
                if (data != null) {

                    temp.setText(String.format("%d\u00B0c", Converter.KelvinToCelcius((float) data.getMain().getTemp())));
                    if (Converter.KelvinToCelcius((float) data.getMain().getTemp_min()) == Converter.KelvinToCelcius((float) data.getMain().getTemp_max())) {
                        min_temp.setText(String.format("%d\u00B0", Converter.KelvinToCelcius((float) data.getMain().getTemp_min()) - Utility.randomNum(6)));
                    } else {
                        min_temp.setText(String.format("%d\u00B0", Converter.KelvinToCelcius((float) data.getMain().getTemp_min())));
                    }

                    max_temp.setText(String.format("%d\u00B0", Converter.KelvinToCelcius((float) data.getMain().getTemp_max())));
                    weatherStatus.setText(data.getWeather().get(0).getDescription().toUpperCase());
                    windSpeed.setText(String.format("Wind %dm/s", Math.round(data.getWind().getSpeed())));
                    humidity.setText(String.format("H%d%%", data.getMain().getHumidity()));
                    pressure.setText(String.format("Pressure: %dhPa", Math.round(data.getMain().getPressure())));
                    cloud.setText(String.format("Cloud: %d%%", data.getClouds().getAll()));
                    sunrise.setText(String.format("Sunrise: %s", Utility.getTime(data.getSys().getSunrise(), timeZone)));
                    sunset.setText(String.format("Sunset: %s", Utility.getTime(data.getSys().getSunset(), timeZone)));

                    //  Log.d("Json size %d", String.valueOf(Utility.getTimeZoneList(Utility.getJsonString(getActivity())).length));


                }


            }

            @Override
            public void onFailure(Call<MainData> call, @NonNull Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    dialog.dismiss();
                    swipe.setRefreshing(false);
                    ShowSnackBar("Connection timeout");

                }
            }

        });


    }

    public void getDailyData(double lat, double lon, int cmt) {
        GetResponse response = RetrofitClient.getRetrofit().create(GetResponse.class);
        Call<GetDailyWeather> call = response.getMultiWeather(lon, lat, cmt, Utility.appid);
        call.enqueue(new Callback<GetDailyWeather>() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onResponse(Call<GetDailyWeather> call, Response<GetDailyWeather> response) {

                GetDailyWeather weather = response.body();
                if (weather != null) {
                    country.setText(String.format("Country: %s", Converter.GpToName(weather.getCity().getCountry())));
                    population.setText(String.format("Population: %d", weather.getCity().getPopulation()));
                    list = weather.getList();
                    adapter = new Horizontal_recycle_adapter(list, getActivity());
                    recyclerView.setAdapter(adapter);
                    dialog.dismiss();
                    swipe.setRefreshing(false);

                } else {
                    Log.d("error at daily weather", "null body");
                }
            }

            @Override
            public void onFailure(Call<GetDailyWeather> call, Throwable t) {

            }
        });
    }

    public void FiveDayData(double lat, double lon) {
        GetResponse response = RetrofitClient.getRetrofit().create(GetResponse.class);
        Call<GetFiveDayData> call = response.get5DaysData(lon, lat, Utility.appid);
        call.enqueue(new Callback<GetFiveDayData>() {

            @Override
            public void onResponse(Call<GetFiveDayData> call, Response<GetFiveDayData> response) {
                GetFiveDayData data = response.body();

                if (data != null) {

                    List<GetFiveDayData.FiveDayMain> list = data.getList();
                    List<List<GetFiveDayData.FiveDayMain>> gotData = Utility.getExecute(list);
                    Collections.sort(gotData, new Comparator<List<GetFiveDayData.FiveDayMain>>() {
                        @Override
                        public int compare(List<GetFiveDayData.FiveDayMain> fiveDayMains, List<GetFiveDayData.FiveDayMain> t1) {
                            return Integer.valueOf(fiveDayMains.get(0).getDt()).compareTo(t1.get(0).getDt());
                        }
                    });
                    vertical_recyle_adapter = new Vertical_recyle_adapter(getActivity(), gotData, currentLocation);
                    vertical_recyle.setAdapter(vertical_recyle_adapter);

                }

            }

            @Override
            public void onFailure(Call<GetFiveDayData> call, Throwable t) {

            }
        });

    }

    @Override
    public void onRefresh() {
        if (isDataConnected(getActivity())) {
            getMainData(locationPojo.getLatitude(), locationPojo.getLongitude());
            getDailyData(locationPojo.getLatitude(), locationPojo.getLongitude(), 10);
            FiveDayData(locationPojo.getLatitude(), locationPojo.getLongitude());
        } else {
            ShowSnackBar("No internet connection");
            swipe.setRefreshing(false);
        }

    }

    public void ShowSnackBar(String msg) {
        Snackbar snackbar = Snackbar.make(getView().findViewById(R.id.show_data_layout), msg, Snackbar.LENGTH_LONG);
        snackbar.setAction("RETRY", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDataConnected(getActivity())) {
                    getMainData(locationPojo.getLatitude(), locationPojo.getLongitude());
                } else {
                    ShowSnackBar("No internet connection");
                    swipe.setRefreshing(false);
                }
            }
        });
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView showView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        showView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
}
