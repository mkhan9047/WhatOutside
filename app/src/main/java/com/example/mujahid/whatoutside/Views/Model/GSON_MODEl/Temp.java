package com.example.mujahid.whatoutside.Views.Model.GSON_MODEl;

/**
 * Created by Mujahid on 4/6/2018.
 */

public class Temp {
    double day;
    double min;
    double max;
    double night;
    double eve;
    double morn;

    public double getDay() {
        return day;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getNight() {
        return night;
    }

    public double getEve() {
        return eve;
    }

    public double getMorn() {
        return morn;
    }

    public Temp(double day, double min, double max, double night, double eve, double morn) {
        this.day = day;
        this.min = min;
        this.max = max;
        this.night = night;
        this.eve = eve;
        this.morn = morn;
    }
}
