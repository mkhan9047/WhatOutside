package com.example.mujahid.whatoutside.Views.REST_Client;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mujahid on 2/25/2018.
 */

public class RetrofitClient {

    private static final String BaseURl = "http://api.openweathermap.org/";
    private static final String TimeZoneBaseUrl = "https://maps.googleapis.com/";

    public static Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl(BaseURl).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static Retrofit getRetrofitForTimeZone() {
        return new Retrofit.Builder().baseUrl(TimeZoneBaseUrl).addConverterFactory(GsonConverterFactory.create()).build();
    }


}
