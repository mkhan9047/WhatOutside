package com.example.mujahid.whatoutside.Views.Model.GSON_MODEl;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mujahid on 4/11/2018.
 */

public class GetFiveDayData {

    private List<FiveDayMain> list;

    public GetFiveDayData(List<FiveDayMain> list) {
        this.list = list;
    }

    public List<FiveDayMain> getList() {
        return list;
    }

    public static class FiveDayMain implements Serializable {
        List<Weathers> weather;
        int dt;
        Main main;

        public FiveDayMain(List<Weathers> weather, int dt, Main main) {
            this.weather = weather;
            this.dt = dt;
            this.main = main;
        }

        public List<Weathers> getWeather() {
            return weather;
        }

        public int getDt() {
            return dt;
        }

        public Main getMain() {
            return main;
        }

        public static class Main implements Serializable {

            private double temp;
            private double pressure;
            private int humidity;
            private double temp_min;
            private double temp_max;
            private double sea_level;

            public double getTemp() {
                return temp;
            }

            public double getPressure() {
                return pressure;
            }

            public int getHumidity() {
                return humidity;
            }

            public double getTemp_min() {
                return temp_min;
            }

            public double getTemp_max() {
                return temp_max;
            }

            public double getSea_level() {
                return sea_level;
            }

            public Main(double temp, double pressure, int humidity, double temp_min, double temp_max, double sea_level) {
                this.temp = temp;
                this.pressure = pressure;
                this.humidity = humidity;
                this.temp_min = temp_min;
                this.temp_max = temp_max;
                this.sea_level = sea_level;
            }
        }

    }

}
