package com.example.mujahid.whatoutside.Views.Model.GSON_MODEl;

import java.util.List;

/**
 * Created by Mujahid on 3/26/2018.
 */

public class MainData {

    private TempData main;
    private List<Weather> weather;
    private Wind wind;
    private Clouds clouds;
    private Extra sys;

    public MainData(TempData main, List<Weather> weather, Wind wind, Clouds clouds, Extra sys) {
        this.main = main;
        this.weather = weather;
        this.wind = wind;
        this.clouds = clouds;
        this.sys = sys;
    }


    public TempData getMain() {
        return main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public Wind getWind() {
        return wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public Extra getSys() {
        return sys;
    }

    public static class Weather {

        String description;
        String icon;

        public Weather(String description, String icon) {
            this.description = description;
            this.icon = icon;
        }

        public String getDescription() {
            return description;
        }

        public String getIcon() {
            return icon;
        }


    }

    public static class TempData {
        double temp;
        double pressure;
        int humidity;
        double temp_min;
        double temp_max;
        double sea_level;
        double grnd_level;

        public TempData(double temp, double pressure, int humidity, double temp_min, double temp_max, double sea_level, double grnd_level) {
            this.temp = temp;
            this.pressure = pressure;
            this.humidity = humidity;
            this.temp_min = temp_min;
            this.temp_max = temp_max;
            this.sea_level = sea_level;
            this.grnd_level = grnd_level;
        }

        public double getTemp() {
            return temp;
        }

        public double getPressure() {
            return pressure;
        }

        public int getHumidity() {
            return humidity;
        }

        public double getTemp_min() {
            return temp_min;
        }

        public double getTemp_max() {
            return temp_max;
        }

        public double getSea_level() {
            return sea_level;
        }

        public double getGrnd_level() {
            return grnd_level;
        }
    }

    public class Wind {
        double speed;

        public Wind(double speed) {
            this.speed = speed;
        }

        public double getSpeed() {
            return speed;
        }
    }

    public static class Clouds {
        int all;

        public Clouds(int all) {
            this.all = all;
        }

        public int getAll() {
            return all;
        }
    }

    public static class Extra {
        int sunrise;
        int sunset;

        public Extra(int sunrise, int sunset) {
            this.sunrise = sunrise;
            this.sunset = sunset;
        }

        public int getSunrise() {
            return sunrise;
        }

        public int getSunset() {
            return sunset;
        }
    }
}
