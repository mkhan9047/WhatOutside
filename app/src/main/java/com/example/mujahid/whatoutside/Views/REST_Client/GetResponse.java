package com.example.mujahid.whatoutside.Views.REST_Client;

import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetDailyWeather;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetFiveDayData;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.MainData;
import com.example.mujahid.whatoutside.Views.Model.LatLon;
import com.example.mujahid.whatoutside.Views.Model.TimeZonePojo;

import java.net.URLEncoder;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Mujahid on 2/25/2018.
 */

public interface GetResponse {
    @GET("/data/2.5/weather")
    Call<MainData> getWeather(@Query("lon") double lon, @Query("lat") double lat, @Query("appid") String appid);

    @GET("/data/2.5/forecast/daily")
    Call<GetDailyWeather> getMultiWeather(@Query("lon") double lon, @Query("lat") double lat, @Query("cnt") int cn, @Query("appid") String appid);

    @GET("/data/2.5/forecast")
    Call<GetFiveDayData> get5DaysData(@Query("lon") double lon, @Query("lat") double lat, @Query("appid") String appid);

    @GET("/maps/api/timezone/json")
    Call<TimeZonePojo> getTimeZone(@Query("location") LatLon location, @Query("timestamp") long timestamp, @Query("key") String key);

}
