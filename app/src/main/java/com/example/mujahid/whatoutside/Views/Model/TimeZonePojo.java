package com.example.mujahid.whatoutside.Views.Model;

import java.util.List;

/**
 * Created by Mujahid on 4/29/2018.
 */

public class TimeZonePojo {

    private String timeZoneId;
    private String status;

    public TimeZonePojo(String timeZoneId, String status) {
        this.timeZoneId = timeZoneId;
        this.status = status;
    }


    public String getTimeZoneId() {
        return timeZoneId;
    }

    public String getStatus() {
        return status;
    }
}
