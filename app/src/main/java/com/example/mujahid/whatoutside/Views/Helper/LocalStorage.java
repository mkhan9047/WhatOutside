package com.example.mujahid.whatoutside.Views.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by Mujahid on 2/26/2018.
 */

public class LocalStorage {

    private Context context;
    public static final String PressureDefault = "m/hPa";
    public static final String WindDefault = "km/h";
    public static final String DateFormatDefault = "dd/mm/yyyy";
    public static final int TempFormatDefault = 1;
    public static final int TimeFormatDefault = 0;
    public static final boolean isFirstTimeDeafult = true;
    public static final boolean isPermissionGuranted = false;
    public LocalStorage(Context c) {
        this.context = c;
    }

    private SharedPreferences.Editor getPreferencesEditor() {
        return getsharedPreferences().edit();
    }

    public void savePressureState(String p) {
        getPreferencesEditor().putString("pressure", p).commit();

    }

    public void savePermissionGuranted(Boolean b) {
        getPreferencesEditor().putBoolean("permission", b).commit();
    }

    public void SaveFirstTimeCheker(Boolean b) {
        getPreferencesEditor().putBoolean("FirstTIme", b).commit();
    }

    public void saveWindState(String p) {
        getPreferencesEditor().putString("wind", p).commit();
    }

    public void saveDateFormatState(String p) {
        getPreferencesEditor().putString("date", p).commit();
    }

    public void saveTempState(int p) {
        getPreferencesEditor().putInt("temp", p).commit();
    }

    public void saveTimeFormatState(int p) {
        getPreferencesEditor().putInt("time", p).commit();
    }

    private SharedPreferences getsharedPreferences() {

        return context.getSharedPreferences("MyData", Context.MODE_PRIVATE);
    }

    public String getPressureState() {

        return getsharedPreferences().getString("pressure", PressureDefault);
    }

    public String getWindState() {
        return getsharedPreferences().getString("wind", WindDefault);
    }

    public String getDateFormatState() {
        return getsharedPreferences().getString("date", DateFormatDefault);
    }

    public int getTimeFormatState() {
        return getsharedPreferences().getInt("time", TimeFormatDefault);
    }

    public int getTempFormatState() {
        return getsharedPreferences().getInt("temp", TempFormatDefault);
    }

    public boolean isFirstTime() {
        return getsharedPreferences().getBoolean("FirstTime", isFirstTimeDeafult);
    }

    public boolean IsPermissionGuranted() {
        return getsharedPreferences().getBoolean("permission", isPermissionGuranted);
    }
}
