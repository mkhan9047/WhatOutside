package com.example.mujahid.whatoutside.Views.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Custom_Adapters.FiveDayDetailAdapter;
import com.example.mujahid.whatoutside.Views.Helper.Utility;
import com.example.mujahid.whatoutside.Views.Model.FiveDayPojo;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetFiveDayData;

import java.util.ArrayList;
import java.util.List;

public class FiveDayDetail extends AppCompatActivity {

    RecyclerView recyclerView;
    FiveDayDetailAdapter adapter;
    List<GetFiveDayData.FiveDayMain> list;
    ImageView imageView;
    String currentLocation;
    @SuppressLint("SetTextI18n")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fiveday_cordinator);
        Toolbar toolbar = findViewById(R.id.fiveday_toolbar);
        TextView title = toolbar.findViewById(R.id.fiveday_toolbar_title);
        TextView subTitle = toolbar.findViewById(R.id.fiveday_sub_title);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        recyclerView = findViewById(R.id.fiveday_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Intent i = getIntent();
        list = (List<GetFiveDayData.FiveDayMain>) i.getSerializableExtra("list");
        currentLocation = i.getStringExtra("location");
        title.setText(currentLocation);
        subTitle.setText(String.format("3 hours forecast of %s", Utility.DateFDate(list.get(0).getDt())));
        adapter = new FiveDayDetailAdapter(list, this);
        recyclerView.setAdapter(adapter);

    }
}
