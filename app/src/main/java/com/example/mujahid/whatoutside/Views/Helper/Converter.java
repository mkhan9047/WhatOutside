package com.example.mujahid.whatoutside.Views.Helper;

import java.util.Locale;

/**
 * Created by Mujahid on 2/27/2018.
 */

public class Converter {

    public static int KelvinToCelcius(float temp) {
        return Math.round(temp) - 273;
    }

    public static int CelciusToFaranhit(int temp) {
        return (temp * 273) / 80;
    }

    public static String StrinTrimer(String s) {

        String[] keep = s.split(" ");
        StringBuilder builder = new StringBuilder();
        if (keep.length >= 2) {
            builder.append(keep[1]);
            builder.append(keep[2]);
        } else {
            builder.append(s);
        }
        return builder.toString();
    }

    public static String GpToName(String code) {
        Locale locale = new Locale("", code);
        return locale.getDisplayCountry();
    }

}
