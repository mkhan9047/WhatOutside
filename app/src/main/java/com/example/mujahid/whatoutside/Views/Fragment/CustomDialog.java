package com.example.mujahid.whatoutside.Views.Fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Activity.MainActivity;
import com.example.mujahid.whatoutside.Views.Helper.LocalStorage;
import java.util.ArrayList;

import belka.us.androidtoggleswitch.widgets.BaseToggleSwitch;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;

/**
 * Created by Mujahid on 2/25/2018.
 */

public class CustomDialog extends DialogFragment {

    TextView pressure, wind, date;
    ToggleSwitch tempSwtich;
    ToggleSwitch hourSwtich;
    LinearLayout pressureLayout, windLayout, dateLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.dialog_custom, container, false);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle b) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View in = inflater.inflate(R.layout.dialog_custom, null);
        //pressure added
        tempSwtich = in.findViewById(R.id.temp_switch);
        hourSwtich = in.findViewById(R.id.hour_switch);
        pressure = in.findViewById(R.id.pressure_unit);
        pressureLayout = in.findViewById(R.id.pressureLayout);
        windLayout = in.findViewById(R.id.wind_layout);
        dateLayout = in.findViewById(R.id.date_layout);
        windLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] windUnit = new String[3];
                windUnit[0] = "Km/h";
                windUnit[1] = "m/s";
                windUnit[2] = "mm/h";
                dialog(windUnit, "Choose Wind Unit");
            }
        });
        dateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] windUnit = new String[3];
                windUnit[0] = "mm/dd/yyyy";
                windUnit[1] = "dd/mm/yyyy";
                windUnit[2] = "yyyy/mm/dd";
                dialog(windUnit, "Choose Date Format");
            }
        });
        pressureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] pressurearray = new String[3];
                pressurearray[0] = "m/hPa";
                pressurearray[1] = "k/hPa";
                pressurearray[2] = "c/hPa";
                dialog(pressurearray, "Choose Pressure Unit");
            }
        });
        wind = in.findViewById(R.id.wind_format);
        date = in.findViewById(R.id.date_format);
        final LocalStorage storage = new LocalStorage(getActivity());
        if (storage.getPressureState().equals(LocalStorage.PressureDefault)) {
            pressure.setText(LocalStorage.PressureDefault);
        } else {
            pressure.setText(storage.getPressureState());
        }
        //end of pressure state
        if (storage.getDateFormatState().equals(LocalStorage.DateFormatDefault)) {
            date.setText(LocalStorage.DateFormatDefault);
        } else {
            date.setText(storage.getDateFormatState());
        }

        //end of date state

        if (storage.getWindState().equals(LocalStorage.WindDefault)) {
            wind.setText(LocalStorage.WindDefault);
        } else {
            wind.setText(storage.getWindState());
        }




        ArrayList<String> lebel = new ArrayList<>();
        lebel.add("F \u00B0");
        lebel.add("C \u00B0");
        tempSwtich.setLabels(lebel);
        if (storage.getTempFormatState() == LocalStorage.TempFormatDefault) {
            tempSwtich.setCheckedTogglePosition(LocalStorage.TempFormatDefault);
        } else {
            tempSwtich.setCheckedTogglePosition(storage.getTempFormatState());
        }


        //hour toogle

        ArrayList<String> lebel2 = new ArrayList<>();
        lebel2.add("12H");
        lebel2.add("24H");
        hourSwtich.setLabels(lebel2);
        if (storage.getTimeFormatState() == LocalStorage.TimeFormatDefault) {
            hourSwtich.setCheckedTogglePosition(LocalStorage.TimeFormatDefault);
        } else {
            hourSwtich.setCheckedTogglePosition(storage.getTimeFormatState());
        }


        hourSwtich.setOnToggleSwitchChangeListener(new BaseToggleSwitch.OnToggleSwitchChangeListener() {
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {

                storage.saveTimeFormatState(position);
            }
        });

        //temp toogle

        tempSwtich.setOnToggleSwitchChangeListener(new BaseToggleSwitch.OnToggleSwitchChangeListener() {
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {

                storage.saveTempState(position);

            }
        });

        builder.setView(in);
        builder.setNeutralButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        return builder.create();
    }

    void dialog(final String[] dataHolder, final String Title) {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
        //alt_bld.setIcon(R.drawable.icon);
        alt_bld.setTitle(Title);
        alt_bld.setSingleChoiceItems(dataHolder, -1, new DialogInterface
                .OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                LocalStorage storage = new LocalStorage(getActivity());
                if (Title.contains("Pressure")) {
                    storage.savePressureState(dataHolder[item]);
                    pressure.setText(dataHolder[item]);

                } else if (Title.contains("Wind")) {
                    storage.saveWindState(dataHolder[item]);
                    wind.setText(dataHolder[item]);

                } else if (Title.contains("Date")) {
                    storage.saveDateFormatState(dataHolder[item]);
                    date.setText(dataHolder[item]);

                }
                // newFragment.getDialog().notify();
                dialog.dismiss();// dismiss the alertbox after chose option

            }
        });

        AlertDialog alert = alt_bld.create();
        alert.show();


///// grpname is a array where data is stored...


    }
}
