package com.example.mujahid.whatoutside.Views.Custom_Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Helper.Utility;
import com.example.mujahid.whatoutside.Views.Model.FiveDayPojo;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetFiveDayData;

import java.util.List;

/**
 * Created by Mujahid on 3/9/2018.
 */

public class FiveDayDetailAdapter extends RecyclerView.Adapter<FiveDayDetailAdapter.MyViewHolder> {

    List<GetFiveDayData.FiveDayMain> list;
    Context context;

    public FiveDayDetailAdapter(List<GetFiveDayData.FiveDayMain> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_fiveday_detail_recyle, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.temp.setText(String.format("%s\u00B0", String.format("%d", Math.round(list.get(position).getMain().getTemp() - 273.15))));
        holder.imageView.setImageResource(R.mipmap.w1d);
        holder.humadity.setText(String.format("Humadity: %d%%", list.get(position).getMain().getHumidity()));
        holder.wind.setText(String.format("Sea Level: %.2f m/hPa", list.get(position).getMain().getSea_level()));
        holder.time.setText(Utility.EpochToTime(list.get(position).getDt()));
        holder.sky.setText(list.get(position).getWeather().get(0).getDescription());
        holder.pressure.setText(String.format("Pressure: %.2f", list.get(position).getMain().getPressure()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView time, pressure, wind, humadity, sky, temp;
        ImageView imageView;

        public MyViewHolder(View itemView) {

            super(itemView);
            time = itemView.findViewById(R.id.fiveday_time);
            pressure = itemView.findViewById(R.id.fiveday_pressure);
            wind = itemView.findViewById(R.id.fiveday_wind);
            humadity = itemView.findViewById(R.id.fiveday_humadity);
            sky = itemView.findViewById(R.id.fiveday_sky);
            temp = itemView.findViewById(R.id.fiveday_temp);
            imageView = itemView.findViewById(R.id.fiveday_image);
        }
    }
}
