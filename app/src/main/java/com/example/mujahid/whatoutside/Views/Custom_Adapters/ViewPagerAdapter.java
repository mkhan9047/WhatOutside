package com.example.mujahid.whatoutside.Views.Custom_Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.example.mujahid.whatoutside.Views.Model.ManageLocationPojo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by Mujahid on 2/10/2018.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> fragmentArrayList = new ArrayList<>();
    private long base = 0;

    public void addFragment(Fragment fragment) {

        fragmentArrayList.add(fragment);

    }

    public void addFragment(int index, Fragment fragment) {
        fragmentArrayList.add(index, fragment);
    }

    public void AddAll(List<Fragment> l) {

        fragmentArrayList.addAll(l);

    }

    public void Remove() {
        fragmentArrayList.remove(fragmentArrayList.size() - 1);
    }

    public void Swapper() {
        Fragment keeper = fragmentArrayList.get(fragmentArrayList.size() - 1);
        fragmentArrayList.add(fragmentArrayList.size() - 1, fragmentArrayList.get(fragmentArrayList.size() - 2));
        fragmentArrayList.add(fragmentArrayList.size() - 2, keeper);
    }

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArrayList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}
