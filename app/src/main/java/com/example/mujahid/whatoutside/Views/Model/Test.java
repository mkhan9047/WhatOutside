package com.example.mujahid.whatoutside.Views.Model;

/**
 * Created by Mujahid on 2/11/2018.
 */

public class Test {
    private int dt;
    private double temp;
    private int image;

    public int getDt() {
        return dt;
    }

    public double getTemp() {
        return temp;
    }

    public int getImage() {
        return image;
    }

    public Test(int dt, double temp, int image) {
        this.dt = dt;
        this.temp = temp;
        this.image = image;
    }
}
