package com.example.mujahid.whatoutside.Views.Model;

/**
 * Created by Mujahid on 2/20/2018.
 */

public class Test2 {

    private String date_time;
    private int image;
    private int temp_min;
    private int temp_max;

    public String getDate_time() {
        return date_time;
    }

    public int getImage() {
        return image;
    }

    public int getTemp_min() {
        return temp_min;
    }

    public int getTemp_max() {
        return temp_max;
    }

    public Test2(String date_time, int image, int temp_min, int temp_max) {

        this.date_time = date_time;
        this.image = image;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
    }
}
