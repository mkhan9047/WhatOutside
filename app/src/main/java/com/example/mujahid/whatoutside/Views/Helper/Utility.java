package com.example.mujahid.whatoutside.Views.Helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.example.mujahid.whatoutside.Views.Fragment.show_data;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetFiveDayData;
import com.example.mujahid.whatoutside.Views.Model.TimeZonePojo;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

/**
 * Created by Mujahid on 3/26/2018.
 */

public class Utility {



    //constants
    public static final String appid = "49e205a0f7848605b41aa220e12788f3";
    //Googgole TIme Zone API key
    //AIzaSyD0xcioJavbg3sdBPNylLK8aMBX6JXHj3U
    public static final String Internet = "android.net.conn.CONNECTIVITY_CHANGE";

    public static String getCurrentDate(Context context) {

        Calendar calendar = Calendar.getInstance();
        LocalStorage storage = new LocalStorage(context);
        SimpleDateFormat dateFormat = null;
        String keep = null;

        switch (storage.getDateFormatState()) {
            case "yyyy/mm/dd":
                dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
                break;

            case "dd/mm/yyyy":
                dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                break;

            case "mm/dd/yyyy":
                dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
                break;
        }
        if (dateFormat != null) {
            keep = dateFormat.format(calendar.getTime());
        }

        return keep;
    }

    public static String getCurrentDay() {
        SimpleDateFormat formater = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance();
        return formater.format(calendar.getTime()).toUpperCase();
    }

    public static String getTime(int dt, String timeZone) {
        Date date = new Date(dt * 1000L);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        if (timeZone == null) {
            timeZone = TimeZone.getDefault().getID();
        }
        TimeZone timeZone1 = TimeZone.getTimeZone(timeZone);
        Log.d("TimeZoneName", timeZone);
        dateFormat.setTimeZone(timeZone1);
        return dateFormat.format(date);

    }



    public static List<List<GetFiveDayData.FiveDayMain>> getExecute(List<GetFiveDayData.FiveDayMain> input) {
        ArrayList<String> actualDate = new ArrayList<>();
        Set<String> keep = new HashSet<>();

        for (int i = 0; i < input.size(); i++) {

            actualDate.add(DateFormater(input.get(i).getDt()));
        }

        keep.addAll(actualDate);
        actualDate.clear();
        actualDate.addAll(keep);

        List<Date> list = new ArrayList<>();
        for (int d = 0; d < actualDate.size(); d++) {
            Date test = null;
            try {

                test = new SimpleDateFormat("yyyy-MM-dd").parse(actualDate.get(d));
                list.add(test);

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        Collections.sort(list, new Comparator<Date>() {
            @Override
            public int compare(Date date, Date t1) {
                return Integer.valueOf(date.getDate()).compareTo(t1.getDate());
            }
        });

        final ArrayList<GetFiveDayData.FiveDayMain> data = new ArrayList<>();
        //   final ArrayList<ArrayList<Wdata>> container = new ArrayList<>();

        final List<GetFiveDayData.FiveDayMain> second = new ArrayList<>();
        final List<GetFiveDayData.FiveDayMain> third = new ArrayList<>();
        final List<GetFiveDayData.FiveDayMain> fourth = new ArrayList<>();
        final List<GetFiveDayData.FiveDayMain> fifth = new ArrayList<>();
        final List<GetFiveDayData.FiveDayMain> six = new ArrayList<>();


        for (int i = 0; i < list.size(); i++) {

            //  container.add(data);
            switch (i) {
                case 0:
                    second.addAll(data);
                    break;
                case 1:
                    second.addAll(data);
                    break;
                case 2:
                    third.addAll(data);
                    break;
                case 3:
                    fourth.addAll(data);
                    break;
                case 4:
                    fifth.addAll(data);
                    break;
                case 5:
                    six.addAll(data);
                    break;
            }

            if (i != 5) {
                data.clear();
            }


            for (int j = 0; j < input.size(); j++) {
                if (DateMake(input.get(j).getDt()).getDate() == list.get(i).getDate()) {
                    data.add(input.get(j));
                }
            }
        }

        List<List<GetFiveDayData.FiveDayMain>> output = new ArrayList<>();
        if (second.size() > 0) {
            output.add(second);
        }
        if (third.size() > 0) {
            output.add(third);
        }
        if (fourth.size() > 0) {
            output.add(fourth);
        }
        if (fifth.size() > 0) {
            output.add(fifth);
        }
        if (six.size() > 0) {
            output.add(six);
        }


        return output;

    }

    private static String DateFormater(int unix) {
        Date a = new Date(unix * 1000L);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
        return formater.format(a);
    }

    private static Date DateMake(int unix) {
        return new Date(unix * 1000L);
    }

    public static String DateFDate(int d) {
        Date a = new Date(d * 1000L);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("EEEE");
        return format.format(a);
    }

    public static String EpochToTime(int d) {
        Date a = new Date(d * 1000L);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        return format.format(a);
    }


    public static int randomNum(int bet) {
        return (int) (Math.random() * bet + 1);
    }

    public static String getCountryName(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                return addresses.get(0).getCountryName();
            }
        } catch (IOException ignored) {
        }
        return Locale.getDefault().getCountry();
    }


    public static String getJsonString(Context context) {
        String json;
        try {
            InputStream inputStream = context.getAssets().open("timezone.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return json;
    }


    public static TimeZonePojo[] getTimeZoneList(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, TimeZonePojo[].class);
    }




/*
    private static TimeZone getTimeZone(TimeZonePojo[] array, String CountryId){

        List<TimeZonePojo> list = new ArrayList<>();
        Collections.addAll(list, array);
        for(TimeZonePojo pojo : list){
            if(pojo.getCountryName().contains(CountryId)){
                return TimeZone.getTimeZone(pojo.getTimeZones().get(0));
            }
        }

        return null;

    } */


}
