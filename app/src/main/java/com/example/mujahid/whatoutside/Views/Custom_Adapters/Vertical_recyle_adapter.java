package com.example.mujahid.whatoutside.Views.Custom_Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Activity.FiveDayDetail;
import com.example.mujahid.whatoutside.Views.Helper.Utility;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetFiveDayData;
import com.example.mujahid.whatoutside.Views.Model.Test2;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Mujahid on 2/20/2018.
 */

public class Vertical_recyle_adapter extends RecyclerView.Adapter<Vertical_recyle_adapter.VerticalViewHolder> {

    private Context context;
    private List<List<GetFiveDayData.FiveDayMain>> list;
    private String currentLocation;

    public Vertical_recyle_adapter(Context context, List<List<GetFiveDayData.FiveDayMain>> list, String currentLocation) {
        this.context = context;
        this.list = list;
        this.currentLocation = currentLocation;
    }

    @Override
    public Vertical_recyle_adapter.VerticalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_recycle, parent, false);
        return new VerticalViewHolder(view);
    }


    private int getLargeTemp(List<GetFiveDayData.FiveDayMain> data) {
        int large = (int) Math.round(data.get(0).getMain().getTemp());

        for (int i = 0; i < data.size(); i++) {
            if (large < Math.round(data.get(i).getMain().getTemp())) {
                large = (int) Math.round(data.get(i).getMain().getTemp());
            }
        }
        return large;
    }

    private int getSamllTemp(List<GetFiveDayData.FiveDayMain> data) {
        int small = (int) Math.round(data.get(0).getMain().getTemp());

        for (int i = 0; i < data.size(); i++) {
            if (small > Math.round(data.get(i).getMain().getTemp())) {
                small = (int) Math.round(data.get(i).getMain().getTemp());
            }
        }
        return small;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(VerticalViewHolder holder, int position) {

        holder.date_time.setText(Utility.DateFDate(list.get(position).get(0).getDt()).toUpperCase());
        holder.min_temp.setText(String.format("%s\u00B0", String.format("%d", getSamllTemp(list.get(position)) - 273)));
        holder.max_temp.setText(String.format("%s\u00B0", String.format("%d", getLargeTemp(list.get(position)) - 273)));

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    class VerticalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView date_time, max_temp, min_temp;
        ImageView imageView;

        public VerticalViewHolder(View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
            date_time = itemView.findViewById(R.id.date_time);
            max_temp = itemView.findViewById(R.id.max_temp);
            min_temp = itemView.findViewById(R.id.min_temp);
            imageView = itemView.findViewById(R.id.vertical_recyle_image);

        }


        @Override
        public void onClick(View view) {

            if (getAdapterPosition() == 0) {
                Intent intent = new Intent(context, FiveDayDetail.class);
                intent.putExtra("list", (Serializable) list.get(0));
                intent.putExtra("location", currentLocation);
                context.startActivity(intent);
            } else if (getAdapterPosition() == 1) {

                Intent intent = new Intent(context, FiveDayDetail.class);
                intent.putExtra("list", (Serializable) list.get(1));
                intent.putExtra("location", currentLocation);
                context.startActivity(intent);
            } else if (getAdapterPosition() == 2) {

                Intent intent = new Intent(context, FiveDayDetail.class);
                intent.putExtra("list", (Serializable) list.get(2));
                intent.putExtra("location", currentLocation);
                context.startActivity(intent);
            } else if (getAdapterPosition() == 3) {

                Intent intent = new Intent(context, FiveDayDetail.class);
                intent.putExtra("list", (Serializable) list.get(3));
                intent.putExtra("location", currentLocation);
                context.startActivity(intent);
            } else if (getAdapterPosition() == 4) {

                Intent intent = new Intent(context, FiveDayDetail.class);
                intent.putExtra("list", (Serializable) list.get(4));
                intent.putExtra("location", currentLocation);
                context.startActivity(intent);
            }
        }
    }


}
