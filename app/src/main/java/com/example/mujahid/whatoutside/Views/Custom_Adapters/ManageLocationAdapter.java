package com.example.mujahid.whatoutside.Views.Custom_Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Activity.MainActivity;
import com.example.mujahid.whatoutside.Views.Database.DatabaseOperation;
import com.example.mujahid.whatoutside.Views.Model.ManageLocationPojo;

import java.util.List;

/**
 * Created by Mujahid on 3/11/2018.
 */

public class ManageLocationAdapter extends ArrayAdapter<ManageLocationPojo> {

    private Context context;
    private List<ManageLocationPojo> list;
    private int resource;
    TextView textView;
    ImageView button;
    int positionHolder;

    public ManageLocationAdapter(@NonNull Context context, int resource, List<ManageLocationPojo> list) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
        this.resource = resource;

    }

    @NonNull
    public View getView(final int p, final View convetView, @NonNull ViewGroup parent) {
        positionHolder = p;
        LinearLayout NaviView;
        final ManageLocationPojo pojo = list.get(p);
        if (convetView == null) {
            NaviView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(inflater);
            assert layoutInflater != null;
            layoutInflater.inflate(resource, NaviView, true);

        } else {

            NaviView = (LinearLayout) convetView;
            Log.d("latitude test", String.format("%f", pojo.getLatitude()));

        }

        textView = NaviView.findViewById(R.id.location_title);
        button = NaviView.findViewById(R.id.location_delete);
        textView.setText(pojo.getTitle());
        button.setFocusable(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dailog = new AlertDialog.Builder(context);
                dailog.setMessage("Do you want to delete this location?");
                dailog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //delete the loaction from database and referesh the adapter
                        DatabaseOperation.DeleteLocation(context, pojo.getId());
                        list.remove(p);
                        notifyDataSetChanged();
                    }

                });
                dailog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dailog.show();
            }

        });

        return NaviView;
    }


}
