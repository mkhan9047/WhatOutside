package com.example.mujahid.whatoutside.Views.Model.GSON_MODEl;

import java.io.Serializable;

/**
 * Created by Mujahid on 4/6/2018.
 */

public class Weathers implements Serializable {
    String main;
    String description;
    String icon;

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public Weathers(String main, String description, String icon) {

        this.main = main;
        this.description = description;
        this.icon = icon;
    }
}
