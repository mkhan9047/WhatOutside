package com.example.mujahid.whatoutside.Views.Model.GSON_MODEl;

import java.util.List;

/**
 * Created by Mujahid on 4/6/2018.
 */

public class GetDailyWeather {

    private City city;
    private List<DailyWeather> list;

    public GetDailyWeather(City city, List<DailyWeather> list) {
        this.city = city;
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public List<DailyWeather> getList() {
        return list;
    }

    public static class City {
        String country;
        int population;

        public String getCountry() {
            return country;
        }

        public int getPopulation() {
            return population;
        }

        public City(String country, int population) {
            this.country = country;
            this.population = population;
        }
    }

    public static class DailyWeather {
        int dt;
        Temp temp;
        double pressure;
        int humidity;
        List<Weathers> weather;
        int clouds;
        float rain;

        public int getDt() {
            return dt;
        }

        public Temp getTemp() {
            return temp;
        }

        public double getPressure() {
            return pressure;
        }

        public int getHumidity() {
            return humidity;
        }

        public List<Weathers> getWeather() {
            return weather;
        }

        public int getClouds() {
            return clouds;
        }

        public float getRain() {
            return rain;
        }

        public DailyWeather(int dt, Temp temp, double pressure, int humidity, List<Weathers> weather, int clouds, float rain) {
            this.dt = dt;
            this.temp = temp;
            this.pressure = pressure;
            this.humidity = humidity;
            this.weather = weather;
            this.clouds = clouds;
            this.rain = rain;
        }
    }

}
