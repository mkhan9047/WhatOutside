package com.example.mujahid.whatoutside.Views.Model;

/**
 * Created by Mujahid on 2/22/2018.
 */

public class NaviListPojo {

    private String text;
    private int image;

    public String getText() {
        return text;
    }

    public int getImage() {
        return image;
    }

    public NaviListPojo(String text, int image) {
        this.text = text;
        this.image = image;
    }
}
