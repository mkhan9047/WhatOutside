package com.example.mujahid.whatoutside.Views.Fragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Activity.ManageLocation;
import com.example.mujahid.whatoutside.Views.Model.ManageLocationPojo;

import java.io.Serializable;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddLocation extends Fragment {

    LinearLayout layout;
    Switch aSwitch;
    LocationManager service;
    String provider;

    public AddLocation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_location, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (getView() != null) {
            layout = getView().findViewById(R.id.add_location);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ManageLocation.class);
                    startActivity(intent);
                }
            });


        }

    }


}
