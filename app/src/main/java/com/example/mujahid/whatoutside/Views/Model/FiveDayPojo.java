package com.example.mujahid.whatoutside.Views.Model;

/**
 * Created by Mujahid on 3/9/2018.
 */

public class FiveDayPojo {

    int humadity;
    int temp;
    String Time;
    int wind;
    int pressure;
    String skyStatus;
    int image;

    public FiveDayPojo(String time, int humadity, int temp, int wind, int pressure, String skyStatus, int image) {
        this.humadity = humadity;
        this.Time = time;
        this.temp = temp;
        this.wind = wind;
        this.pressure = pressure;
        this.skyStatus = skyStatus;
        this.image = image;
    }


    public int getHumadity() {
        return humadity;
    }

    public String getTime() {
        return Time;
    }

    public int getTemp() {
        return temp;
    }

    public int getWind() {
        return wind;
    }

    public int getPressure() {
        return pressure;
    }

    public String getSkyStatus() {
        return skyStatus;
    }

    public int getImage() {
        return image;
    }


}

