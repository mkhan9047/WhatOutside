package com.example.mujahid.whatoutside.Views.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Custom_Adapters.NaviListCustomAdapter;
import com.example.mujahid.whatoutside.Views.Custom_Adapters.ViewPagerAdapter;
import com.example.mujahid.whatoutside.Views.Database.DatabaseOperation;
import com.example.mujahid.whatoutside.Views.Fragment.AddLocation;
import com.example.mujahid.whatoutside.Views.Fragment.CustomDialog;
import com.example.mujahid.whatoutside.Views.Fragment.show_data;
import com.example.mujahid.whatoutside.Views.Helper.LocalStorage;
import com.example.mujahid.whatoutside.Views.Model.ManageLocationPojo;
import com.example.mujahid.whatoutside.Views.Model.NaviListPojo;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import at.favre.lib.dali.builder.nav.DaliBlurDrawerToggle;
import at.favre.lib.dali.builder.nav.NavigationDrawerListener;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ViewPager pager;
    ViewPagerAdapter adapter;
    ImageView imageView;
    ListView listView;
    List<NaviListPojo> Sitting_list;
    NaviListCustomAdapter Listadapter;

    List<NaviListPojo> DeveloperList;
    NaviListCustomAdapter DeveloperAdapter;
    ListView developerListView;

    DrawerLayout drawer;
    DialogFragment newFragment;

    int PLACE_PICKER_REQUEST = 1;
    //DIalog
    ToggleSwitch tempSwtich;
    ToggleSwitch hourSwtich;

    LocationManager manager;
    String provider;
    boolean enabled;
    GeoDataClient placeClient;
    public TextView mTitle;
    LinkedList<android.support.v4.app.Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        final Toolbar toolbar = findViewById(R.id.toolbar);

        //testToolbar.setTitle("Testing it");
        mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        mTitle.setFocusable(true);
        mTitle.setFocusableInTouchMode(true);
        mTitle.requestFocus();
        mTitle.setSingleLine(true);
        mTitle.setSelected(true);
        mTitle.setMarqueeRepeatLimit(-1);
        imageView = findViewById(R.id.wall_image);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //   mTitle.setAnimation(inFromRightAnimation());

        //permission chcek
        //dialog
        //permission check
        //end of fisrt time checker
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                                  @Override
                                  public void onPermissionsChecked(MultiplePermissionsReport report) {
                                      if (report.areAllPermissionsGranted()) {
                                          LocalStorage storage = new LocalStorage(getApplicationContext());
                                          if (storage.IsPermissionGuranted() == LocalStorage.isPermissionGuranted) {
                                              storage.savePermissionGuranted(true);
                                          }

                                      } else {
                                          showSettingsDialog();
                                      }
                                  }

                                  @Override
                                  public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                                  }
                              }

                ).onSameThread()
                .check();


        placeClient = Places.getGeoDataClient(this, null);


        Sitting_list = new ArrayList<>();
        Sitting_list.add(new NaviListPojo("Home", R.drawable.ic_home));
        Sitting_list.add(new NaviListPojo("Locations", R.drawable.ic_location));
        Sitting_list.add(new NaviListPojo("Lock Screen", R.drawable.ic_lock));
        Sitting_list.add(new NaviListPojo("Notification", R.drawable.ic_notification));
        Sitting_list.add(new NaviListPojo("Status Bar   ", R.drawable.ic_status));
        Sitting_list.add(new NaviListPojo("Weather Radar", R.drawable.ic_radar));
        Sitting_list.add(new NaviListPojo("Change Unit", R.drawable.ic_unit));
        Sitting_list.add(new NaviListPojo("Full Version", R.drawable.ic_paid));

        Listadapter = new NaviListCustomAdapter(this, R.layout.simple_navi_list, Sitting_list);
        listView = findViewById(R.id.setting_list);
        listView.setAdapter(Listadapter);
        listView.setOnItemClickListener(this);
        DeveloperList = new ArrayList<>();
        DeveloperList.add(new NaviListPojo("Rate us", R.drawable.ic_grade_black_24dp));
        DeveloperList.add(new NaviListPojo("About Me", R.drawable.ic_grade_black_24dp));
        DeveloperList.add(new NaviListPojo("Share", R.drawable.ic_grade_black_24dp));
        DeveloperList.add(new NaviListPojo("Report Bug", R.drawable.ic_grade_black_24dp));
        developerListView = findViewById(R.id.developer_list);
        DeveloperAdapter = new NaviListCustomAdapter(this, R.layout.simple_navi_list, DeveloperList);
        developerListView.setAdapter(DeveloperAdapter);


        drawer = findViewById(R.id.drawer_layout);

        DaliBlurDrawerToggle daliBlurDrawerToggle = new DaliBlurDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close, new NavigationDrawerListener() {

            @Override
            public void onDrawerClosed(View view) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }
        });

        drawer.addDrawerListener(daliBlurDrawerToggle);
        daliBlurDrawerToggle.syncState();


        CircleIndicator indicator = findViewById(R.id.indicator);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager = findViewById(R.id.pager);


        fragments = new LinkedList<>();


        //end of populated


        pager.setPageMargin(15);

        if (DatabaseOperation.getLocation(this).size() > 0) {
            String firstTitleKeeper = null;
            for (int i = 0; i < DatabaseOperation.getLocation(this).size(); i++) {
                Bundle bn = new Bundle();
                ManageLocationPojo pojo = DatabaseOperation.getLocation(this).get(i);
                bn.putSerializable("data", pojo);
                if (i == 0) {
                    firstTitleKeeper = pojo.getTitle();
                }
                Log.d("cordinates", String.format("Location: %s and lon: %f and lat %f", pojo.getTitle(), pojo.getLongitude(), pojo.getLatitude()));
                android.support.v4.app.Fragment fragment2 = new show_data();
                fragment2.setArguments(bn);
                adapter.addFragment(fragment2);
            }

            mTitle.setText(firstTitleKeeper);
            Bundle tempBundle = new Bundle();
            tempBundle.putSerializable("data", new ManageLocationPojo("Add Location", 0.0, 0.0));
            android.support.v4.app.Fragment fragment = new AddLocation();
            fragment.setArguments(tempBundle);
            adapter.addFragment(fragment);
            adapter.notifyDataSetChanged();
            Log.d("Database adding", String.format("%d", adapter.getCount()));


        } else {
            Bundle tempBundle = new Bundle();
            tempBundle.putSerializable("data", new ManageLocationPojo("Add Location", 0.0, 0.0));
            android.support.v4.app.Fragment fragment = new AddLocation();
            fragment.setArguments(tempBundle);
            adapter.addFragment(adapter.getCount(), fragment);
            adapter.notifyDataSetChanged();
        }


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Bundle bundle = adapter.getItem(position).getArguments();
                ManageLocationPojo locationPojo = (ManageLocationPojo) bundle.getSerializable("data");
                if (locationPojo != null) {
                    mTitle.setText(locationPojo.getTitle());
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());

    }


    private void wallAnimate() {
        Animation anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_in);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setImageResource(R.mipmap.night_cloud_moon);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageView.setAnimation(anim);
        anim.start();

    }


    //open setting
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void showSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs location permission. You can grant them in app settings.");

        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                showSettingsDialog();
            }
        });
        builder.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @SuppressLint("DefaultLocale")
    public void ImageAnimation(LocalDateTime time) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.location:
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("DefaultLocale")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                final Place place = PlacePicker.getPlace(this, data);
                final LatLng latLng = place.getLatLng();
                Toast.makeText(this, String.format("latitude: %.4f", (float) latLng.latitude), Toast.LENGTH_LONG).show();
                /*
                Bundle a = new Bundle();
                android.support.v4.app.Fragment opening = new show_data();
                a.putSerializable("data",(Serializable) new ManageLocationPojo(place.getName().toString(),latLng.latitude,latLng.longitude));
                opening.setArguments(a);
                */
                Runnable a = new Runnable() {
                    @Override
                    public void run() {
                        DatabaseOperation.SaveLocation(MainActivity.this, latLng.latitude, latLng.longitude, place.getName().toString());
                    }
                };
                Thread thread = new Thread(a);
                thread.start();
                Bundle bundle = new Bundle();
                ManageLocationPojo pojo = new ManageLocationPojo(place.getName().toString(), latLng.latitude, latLng.longitude);
                bundle.putSerializable("data", pojo);
                android.support.v4.app.Fragment fragment = new show_data();
                fragment.setArguments(bundle);
                android.support.v4.app.Fragment temp = adapter.getItem(adapter.getCount() - 1);
                adapter.Remove();
                adapter.addFragment(fragment);
                adapter.addFragment(temp);
                adapter.notifyDataSetChanged();
                pager.setCurrentItem(adapter.getCount() - 2);
                mTitle.setText(pojo.getTitle());
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        final TextView text = view.findViewById(R.id.simple_navi_text);
        final Switch aswitch = view.findViewById(R.id.navi_switch);

        if (text.getText().toString().contains("Home")) {
            //  Toast.makeText(this, text.getText().toString(), Toast.LENGTH_LONG).show();
            wallAnimate();


        }

        if (text.getText().toString().contains("Locations")) {
            Intent intent = new Intent(this, ManageLocation.class);
            startActivity(intent);
        }
        if (text.getText().toString().contains("Change Unit")) {
            FragmentTransaction transactionFragment = this.getSupportFragmentManager().beginTransaction();
            CustomDialog newFragment = new CustomDialog();
            transactionFragment.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transactionFragment.add(newFragment, null).commitAllowingStateLoss();
        }
        if (text.getText().toString().contains("Version")) {
            Intent intent = new Intent(this, FiveDayDetail.class);
            startActivity(intent);
        }

        drawer.closeDrawer(Gravity.START);

    }


    public String CityName(double lat, double lon) {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String cityName = null;
        if (addresses != null) {
            cityName = addresses.get(0).getAddressLine(0);
        }
        return cityName;
    }

    @Override
    protected void onResume() {
        super.onResume();

        int index = getIntent().getIntExtra("index", 0);
        boolean isClicked = getIntent().getBooleanExtra("isClicked", false);
        if (isClicked) {
            pager.setCurrentItem(index);
        }



    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dailog = new AlertDialog.Builder(this);
        dailog.setMessage("Do you want to exit?");
        dailog.setIcon(R.mipmap.ic_launcher_round);
        dailog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

        dailog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dailog.show();
    }
}
