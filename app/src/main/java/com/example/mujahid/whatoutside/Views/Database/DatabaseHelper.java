package com.example.mujahid.whatoutside.Views.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Mujahid on 3/11/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "WeatherLocation";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String locationTable = "CREATE TABLE locations (\n" +
                "location_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "latitude DECIMAL(10,5), \n" +
                "longitude DECIMAL(10,5),\n" +
                "location_name TEXT\n" +
                ");";
        String currentLocation = "CREATE TABLE Currentlocations (\n" +
                "location_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "latitude DECIMAL(10,5), \n" +
                "longitude DECIMAL(10,5),\n" +
                "location_name TEXT\n" +
                ");";
        sqLiteDatabase.execSQL(locationTable);
        sqLiteDatabase.execSQL(currentLocation);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS locations";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }
}
