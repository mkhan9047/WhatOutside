package com.example.mujahid.whatoutside.Views.Model;

import java.io.Serializable;

/**
 * Created by Mujahid on 3/11/2018.
 */

public class ManageLocationPojo implements Serializable {

    private String title;
    private double latitude;
    private double longitude;
    private int id;

    public ManageLocationPojo(String title, double lat, double lon) {
        this.latitude = lat;
        this.longitude = lon;
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getTitle() {
        return title;
    }
}
