package com.example.mujahid.whatoutside.Views.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.mujahid.whatoutside.Views.Model.ManageLocationPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mujahid on 3/11/2018.
 */

public class DatabaseOperation {

    public static void SaveLocation(Context context, double lat, double lon, String locationName) {

        try {
            SQLiteOpenHelper helper = new DatabaseHelper(context);
            SQLiteDatabase database = helper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("latitude", lat);
            contentValues.put("longitude", lon);
            contentValues.put("location_name", locationName);
            database.insert("locations", null, contentValues);
            database.close();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

    }


    public static void SaveCurrentLocation(Context context, double lat, double lon, String locationName) {

        try {
            SQLiteOpenHelper helper = new DatabaseHelper(context);
            SQLiteDatabase database = helper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("latitude", lat);
            contentValues.put("longitude", lon);
            contentValues.put("location_name", locationName);
            database.insert("Currentlocations", null, contentValues);
            database.close();
        } catch (SQLiteException e) {
            e.printStackTrace();

        }


    }

    public static List<ManageLocationPojo> getLocation(Context context) {
        SQLiteOpenHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = null;
        final List<ManageLocationPojo> pjo = new ArrayList<>();
        String sqlQueary = "Select * from locations";
        cursor = database.rawQuery(sqlQueary, null);
        while (cursor.moveToNext()) {
            ManageLocationPojo locationPojo = new ManageLocationPojo(cursor.getString(3), cursor.getDouble(1), cursor.getDouble(2));
            locationPojo.setId(cursor.getInt(0));
            pjo.add(locationPojo);

        }
        database.close();
        cursor.close();

        return pjo;
    }

    public static List<ManageLocationPojo> geCurrenttLocation(Context context) {
        SQLiteOpenHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = null;
        final List<ManageLocationPojo> pjo = new ArrayList<>();
        String sqlQueary = "Select * from Currentlocations";
        cursor = database.rawQuery(sqlQueary, null);
        while (cursor.moveToNext()) {
            ManageLocationPojo locationPojo = new ManageLocationPojo(cursor.getString(3), cursor.getDouble(1), cursor.getDouble(2));
            locationPojo.setId(cursor.getInt(0));
            pjo.add(locationPojo);

        }
        cursor.close();
        database.close();
        return pjo;

    }

    public static void DeleteLocation(Context context, int id) {
        SQLiteOpenHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getReadableDatabase();
        String sqlQuary = "Delete from locations where location_id = " + id + " ;";
        database.execSQL(sqlQuary);
        database.close();
    }

    public static void UpdateLocation(Context context, double lon, double lat, String name) {
        SQLiteOpenHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("latitude", lat);
        contentValues.put("longitude", lon);
        contentValues.put("location_name", name);

        database.update("Currentlocations", contentValues, "location_id =" + 1, null);
        database.close();
    }
}
