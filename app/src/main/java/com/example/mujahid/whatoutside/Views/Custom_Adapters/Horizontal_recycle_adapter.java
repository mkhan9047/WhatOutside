package com.example.mujahid.whatoutside.Views.Custom_Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mujahid.whatoutside.Views.Helper.Converter;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.GetDailyWeather;
import com.example.mujahid.whatoutside.Views.Model.Test;

import com.example.mujahid.whatoutside.R;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Mujahid on 2/11/2018.
 */

public class Horizontal_recycle_adapter extends RecyclerView.Adapter<Horizontal_recycle_adapter.MyViewHolder> {

    private List<GetDailyWeather.DailyWeather> list;
    private Context context;
    String dayKeep;

    public Horizontal_recycle_adapter(List<GetDailyWeather.DailyWeather> list, Context con) {
        context = con;
        this.list = list;
    }

    @Override
    public Horizontal_recycle_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizantal_recycle, parent, false);
        return new Horizontal_recycle_adapter.MyViewHolder(view);
    }


    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(Horizontal_recycle_adapter.MyViewHolder holder, int position) {

        holder.temp.setText(String.format("%s\u00B0", String.format("%d", Converter.KelvinToCelcius(Math.round(list.get(position).getTemp().getMax())))));
        Date date = new Date(list.get(position).getDt() * 1000L);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd MMM");
        holder.imageView.setImageResource(R.mipmap.w1d);
        holder.day.setText(format.format(date));

    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView day, temp;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            day = itemView.findViewById(R.id.day);
            temp = itemView.findViewById(R.id.temp);

        }
    }
}
