package com.example.mujahid.whatoutside.Views.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mujahid.whatoutside.R;
import com.example.mujahid.whatoutside.Views.Custom_Adapters.ManageLocationAdapter;
import com.example.mujahid.whatoutside.Views.Database.DatabaseOperation;
import com.example.mujahid.whatoutside.Views.Model.GSON_MODEl.MainData;
import com.example.mujahid.whatoutside.Views.Model.ManageLocationPojo;
import com.example.mujahid.whatoutside.Views.REST_Client.GetResponse;
import com.example.mujahid.whatoutside.Views.REST_Client.NetworkOperation;
import com.example.mujahid.whatoutside.Views.REST_Client.RetrofitClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageLocation extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listView;
    ManageLocationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_location);
        Toolbar toolbar = findViewById(R.id.location_toolbar);
        toolbar.setTitle("Edit Location");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        listView = findViewById(R.id.location_listView);
        adapter = new ManageLocationAdapter(this, R.layout.custom_location_listview, DatabaseOperation.getLocation(this));
        listView.setAdapter(adapter);
        listView.setFocusable(true);
        listView.setClickable(true);
        listView.setOnItemClickListener(this);
    }


    public String CityName(double lat, double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String cityName = null;
        if (addresses != null) {
            cityName = addresses.get(0).getAddressLine(0);
        }
        return cityName;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("index", i);
        intent.putExtra("isClicked", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        Toast.makeText(this, String.format("%d item cliecked", i), Toast.LENGTH_SHORT).show();
    }


}
